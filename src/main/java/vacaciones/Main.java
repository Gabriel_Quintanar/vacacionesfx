package vacaciones;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.net.URL;

public class Main extends Application {

    private final String mainFXML = "/mainScreen.fxml";

    public static Stage window;

    @Override
    public void start(Stage primaryStage) throws Exception {
        URL url = getClass().getResource(mainFXML);
        Parent root = FXMLLoader.load(url);
        primaryStage.setTitle("Control de Vacaciones");
        Scene scene = new Scene(root);
        scene.getStylesheets().add(getClass().getResource("/styles/style.css")
                .toExternalForm());
        primaryStage.setScene(scene);
        window = primaryStage;
        window.getIcons().add(new Image(getClass().getResourceAsStream(
                "/images/beevaIcon.PNG")));
        window.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
