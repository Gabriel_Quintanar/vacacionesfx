package vacaciones.models;

import javafx.collections.FXCollections;

import java.util.List;

public class DataGen {

    private final List<String> columnNames;
    private List<List<?>> data;


    public DataGen(List<String> columnNames, List<List<?>> data) {
        this.columnNames = columnNames;
        this.data = data;
    }

    public int getNumColumns() {
        return columnNames.size();
    }

    public String getColumnName(int index) {
        return columnNames.get(index);
    }

    public int getNumRows() {
        return data.size();
    }

    public Object getData(int column, int row) {
        return data.get(row).get(column);
    }

    public List<String> getColumnNames() {
        return columnNames;
    }

    public List<List<?>> getData() {
        return data;
    }

    public void setDataRow(int row, List<?> objects) {
        data.set(row, objects);
    }

    public void setDataAsObservable() {
        data = FXCollections.observableArrayList(data);
    }
}
