package vacaciones.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import javafx.util.Duration;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import vacaciones.Main;
import vacaciones.models.DataGen;
import vacaciones.service.ExcelPOI;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Controller implements Initializable {

    private Window window;
    private File selectedFile;
    private ExcelPOI excelPOI;
    private DataGen data;
    private int rowToEdit;

    @FXML
    private TableView table;

    @FXML
    private BorderPane pane;

    @FXML
    private MenuItem saveMI;
    @FXML
    private MenuItem openMI;

    @FXML
    private Button askDayButton;
    @FXML
    private Button addEmployeeButton;

    private Button editarBtn;

    @FXML
    private TextArea log;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        window = Main.window;
        editarBtn = new Button("Editar Tupla");
        editarBtn.setOnAction(e -> {
            excelPOI.modifyRow(rowToEdit, sendNewRow());
            data.setDataRow(rowToEdit, sendNewRow());
            log.appendText("Tupla modificada!\n");
        });
        openMI.setOnAction(event -> {
            openFile();
            readExcelFile(selectedFile);
            setTableData(data);
        });
        saveMI.setOnAction(event -> saveFile());
    }

    private List<?> sendNewRow() {
        return getStreamTextFieldsForEdit().map(t -> t.getText())
                .collect(Collectors.toList());
    }

    private void editRow() {
        rowToEdit = table.getEditingCell()
                .getRow();
        popupEditNode();
        fillEditVbox();
    }

    private void fillEditVbox() {
        for (int i = 0; i < data.getNumColumns(); i++) {
            int index = i;
            getStreamTextFieldsForEdit().skip(i).limit(1)
                    .forEach(node ->
                            node.setText(
                                    data.getData()
                                            .get(rowToEdit)
                                            .get(index).toString())

                    );
        }
    }

    private Stream<TextField> getStreamTextFieldsForEdit() {
        Stream stream = (((VBox) ((ScrollPane) pane.getLeft())
                .getContent())
                .getChildren().stream()
                .filter(node -> node instanceof VBox)
                .flatMap(node -> ((VBox) node).getChildren().stream())
                .filter(node -> node instanceof TextField));
        return stream;
    }

    private void popupEditNode() {
        if (pane.getLeft() != null) {
            getStreamTextFieldsForEdit().forEach(node -> node.setText(""));
        } else {
            ScrollPane scrollPane = generateInnerPane();
            pane.setLeft(scrollPane);
            fadeInNode(pane.getLeft());
        }
    }

    private ScrollPane generateInnerPane() {
        VBox vbox = new VBox(5);
        for (int i = 0; i < data.getNumColumns(); i++) {
            Label label = new Label(data.getColumnName(i));
            TextField textField = new TextField();
            VBox innerVBox = new VBox(2);
            innerVBox.setPadding(new Insets(5));
            innerVBox.getChildren().addAll(label, textField);
            innerVBox.alignmentProperty().setValue(Pos.TOP_LEFT);
            vbox.getChildren().add(innerVBox);
        }
        HBox hBox = new HBox(editarBtn);
        hBox.alignmentProperty().setValue(Pos.CENTER);
        vbox.getChildren().add(hBox);
        ScrollPane scrollPane = new ScrollPane(vbox);
        scrollPane.setPrefWidth(250);
        scrollPane.setMaxWidth(400);
        return scrollPane;
    }

    private void openFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Seleccione Archivo Excel");
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Archivos Excel",
                        "*.xls", "*.xlsx")
        );
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        selectedFile = fileChooser.showOpenDialog(window);
        if (selectedFile == null) {
            log.setText("Error al leer el archivo. No se pudo cargar " +
                    "Archivo.");
        }
    }


    private boolean saveFile() {
        if (selectedFile == null)
            return false;
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Guardar Archivo");
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Archivos Excel",
                        "*.xls", "*.xlsx")
        );
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        selectedFile = fileChooser.showSaveDialog(window);
        if (selectedFile != null) {
            FileOutputStream file;
            try {
                file = new FileOutputStream(selectedFile.getPath());
                excelPOI.saveExcel(file);
                log.appendText("Archivo guardado!\n");
            } catch (FileNotFoundException e) {
                log.setText(e.getMessage());
            }
            return true;
        }
        return false;
    }

    private void readExcelFile(File selectedFile) {
        try {
            excelPOI =
                    new ExcelPOI(selectedFile.getPath());
            log.appendText("Archivo cargado con éxito...\n");
            log.appendText("Leyendo archivo...\n");
            table.getColumns().clear();
            data = excelPOI.getDataGenObject(0);
            data.setDataAsObservable();
            log.appendText("Lectura Completa...\n");
            log.appendText("Cargando a tabla...\n");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
    }

    private void setTableData(DataGen data) {
        for (int i = 0; i < data.getNumColumns() - 1; i++) {
            TableColumn<List<?>, ?> column = new TableColumn<>(data
                    .getColumnName(i));
            int columnIndex = i;
            column.setEditable(true);
            column.setResizable(true);
            column.setSortable(true);
            column.setCellValueFactory(cellData ->
                    new SimpleObjectProperty(
                            cellData.getValue().get(columnIndex))
            );
            table.getColumns().add(column);
        }
        table.setItems((ObservableList) data.getData());
        table.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2)
                editRow();
        });
        table.getSelectionModel().cellSelectionEnabledProperty().set(true);
    }

    private void fadeInNode(Node node) {
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.ZERO,
                        new KeyValue(node.opacityProperty(), 0.0)),
                new KeyFrame(new Duration(1200),
                        new KeyValue(node.opacityProperty(), 1.0)));
        timeline.play();
    }
}
