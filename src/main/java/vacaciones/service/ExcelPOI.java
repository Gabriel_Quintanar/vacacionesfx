package vacaciones.service;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import vacaciones.models.DataGen;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExcelPOI {
    private String filePath;
    private Workbook workbook;
    private Sheet sheet;

    public ExcelPOI(String filePath) throws IOException, InvalidFormatException {
        this.filePath = filePath;
        this.workbook = WorkbookFactory.create(new File(this.filePath));
        this.sheet = workbook.getSheetAt(0);
    }

    public boolean modifyRow(int indexRow, List<?> row) {
        for (int i = 0; i < row.size(); i++) {
            if (row.get(i) instanceof Number) {
                sheet.getRow(indexRow).getCell(i)
                        .setCellValue((Double) row.get(i));
            } else if (row.get(i) instanceof String) {
                sheet.getRow(indexRow).getCell(i)
                        .setCellValue(((String) row.get(i)));
            } else if (row.get(i) instanceof Boolean) {
                sheet.getRow(indexRow).getCell(i)
                        .setCellValue((Boolean) row.get(i));
            } else if (row.get(i) instanceof Date) {
                sheet.getRow(indexRow).getCell(i)
                        .setCellValue((Date) row.get(i));
            } else
                return false;
        }
        return true;
    }

    public void saveExcel(FileOutputStream file) {
        try {
            workbook.write(file);
            file.close();
            closeWorkBook();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getSheetNames() {
        StringBuilder sheetNames = new StringBuilder();
        workbook.forEach(sheet ->
                sheetNames.append(sheet.getSheetName()));
        return sheetNames.toString();
    }

    public DataGen getDataGenObject(int sheetIndex) {
        DataFormatter dataFormatter = new DataFormatter();
        List<List<?>> columnList = new ArrayList<>();
        List<String> headers = new ArrayList<>();
        workbook.getSheetAt(sheetIndex).rowIterator()
                .forEachRemaining(row -> {
                    List rowList = new ArrayList();
                    row.cellIterator().forEachRemaining(cell -> {
                        if (columnList.size() < 1)
                            headers.add(dataFormatter.formatCellValue(cell));
                        else {
                            rowList.add(asignJavaValue(cell));
                        }
                    });
                    columnList.add(rowList);
                });
        columnList.remove(0);
        DataGen datagen = new DataGen(headers,
                columnList);
        return datagen;
    }

    private Object asignJavaValue(Cell cell) {
        switch (cell.getCellTypeEnum()) {
            case _NONE:
                return "";
            case BLANK:
                return "";
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell))
                    return cell.getDateCellValue();
                else
                    return new Double(cell.getNumericCellValue());
            case STRING:
                return cell.getRichStringCellValue().getString();
            case FORMULA:
                return cell.getRichStringCellValue().getString();
            case BOOLEAN:
                return new Boolean(cell.getBooleanCellValue());
            case ERROR:
                return "";
        }
        return null;
    }

    public boolean closeWorkBook() {
        try {
            workbook.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Sheet getSheet() {
        return sheet;
    }

    public boolean rewriteData(int sheetIndex, List<List<?>> data) {
        workbook.getSheetAt(sheetIndex)
                .rowIterator().forEachRemaining(row -> {
            modifyRow(row.getRowNum(),
                    data.get(row.getRowNum()));
        });
        return false;
    }
}
